-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hospital_management_system
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hospital_management_system`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hospital_management_system` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hospital_management_system`;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
  `patient_id` varchar(50) NOT NULL,
  `doctor_id` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `room_number` varchar(50) DEFAULT NULL,
  `description` varchar(191) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`patient_id`,`doctor_id`,`date`),
  KEY `doctor_id` (`doctor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES ('0001','001','Sun May 26 13:33:55 ICT 2019','F41','','200,000 riel'),('0002','0002','Sun May 26 12:56:55 ICT 2019','F123456','Hello World','200,000 riel'),('003','003','Sun May 26 15:18:29 ICT 2019','F125','Hello','200,000 riel');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctors` (
  `id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `skill` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES ('0001','James','Moriarty','Male',45,'Doctor'),('0002','Change','Name','Female',49,'Hello');
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `age` int(11) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `villege` varchar(50) DEFAULT NULL,
  `commune` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room` varchar(50) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `doctor_id` varchar(50) NOT NULL,
  `paid` varchar(50) DEFAULT 'not yet',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES ('0001','SOVANN','Moriarty','Female',50,'0987654321','A','B','C','','','2019-05-26 17:16:35','F-35B','Hello World','200,000 riel','d001','paid'),('00010','Test','Test','Male',45,'1234567890','','','','','','2019-06-10 07:23:14','A001','','200,000 riel','D0001','paid'),('00011','Hello','Test','Female',30,'741852963','','','','','','2019-06-10 07:23:52','A123','','200,000 riel','D003','paid'),('00014','Sopheap','David','Female',23,'1234567890','','','','','','2019-06-14 01:03:50','R001','','200,000 riel','D001','paid'),('00016','Sopheap','David','Male',23,'123456789','','','','','','2019-06-14 01:55:23','R0003','','200,000 riel','D002','paid'),('00017','Chep','Seyha','Male',24,'1234567890','','','','','','2019-06-14 04:42:50','A123','','200,000 riel','D001','paid'),('00019','Dourn','Phalin','Male',26,'1234567890','','','','','','2019-06-14 05:24:17','A002','','200,000 riel','D003','paid'),('0002','SOVANN','Panha','Male',43,'123456789','A','B','C','','','2019-05-26 17:16:35','F-35B','Hello World','200,000 riel','d001','paid'),('00029','Hello','Test','Male',23,'123456789','','','','','','2019-06-14 05:30:07','F001','','200,000 riel','D003','not yet'),('0003','Hello','Panha','Male',41,'123456789','A','B','C','','','2019-05-26 17:16:39','F123','Hello World','200,000 riel','d001','paid'),('00030','SOVANN','Moriarty','Male',26,'789+456123','','','','','','2019-06-19 05:21:46','A002','','200,000 riel','D001','not yet'),('0004','James','Moriarty','Male',24,'123456789','A','B','C','','','2019-05-26 17:15:46','F35','Hello World','200,000 riel','d001','paid'),('0005','Sok','Dara','Male',23,'1234567890','A','B','C','','','2019-05-26 18:13:22','F-22','Hello World','200,000 riel','D001','paid'),('0006','Chep','ChanSeyHa','Male',25,'086373839','','','','','','2019-05-27 05:07:54','A12','','200,000 riel','D001','paid'),('0007','Nhem','ThayHeng','Female',23,'069606631','','','','','','2019-05-28 02:15:52','A112','','200,000 riel','D001','paid'),('0008','Srey','Sokhom','Female',22,'123456789','','','','','','2019-05-28 02:17:13','A123','','200,000 riel','D124','paid'),('0009','Vici','Gaming','Female',23,'098765432','','','','','','2019-05-29 11:08:48','A102','','200,000 riel','D0003','paid');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receipts`
--

DROP TABLE IF EXISTS `receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(50) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receipts`
--

LOCK TABLES `receipts` WRITE;
/*!40000 ALTER TABLE `receipts` DISABLE KEYS */;
INSERT INTO `receipts` VALUES (1,'0001','2019-05-27 09:13:06'),(2,'0001','2019-05-28 11:05:41'),(3,'0002','2019-05-28 11:05:45'),(4,'0003','2019-05-28 11:05:48'),(5,'0008','2019-06-13 11:13:09'),(6,'0005','2019-06-13 11:14:33'),(7,'00010','2019-06-13 11:22:23'),(8,'00014','2019-06-14 01:04:54'),(9,'00017','2019-06-14 04:42:57'),(10,'0007','2019-06-14 04:48:35'),(11,'0006','2019-06-14 05:03:06'),(12,'00016','2019-06-14 05:03:45'),(13,'0009','2019-06-14 05:04:48'),(14,'0004','2019-06-14 05:05:39'),(15,'00019','2019-06-14 05:24:22'),(16,'00011','2019-06-14 05:28:43');
/*!40000 ALTER TABLE `receipts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-20 12:39:06
