package classes;

import java.sql.Timestamp;

public class Receipt {

	private int id;
	private String patient_id;
	private java.sql.Timestamp date;
	//Constructor
	public Receipt() {
		super();
	}

	public Receipt(int id, String patient_id, Timestamp date) {
		super();
		this.id = id;
		this.patient_id = patient_id;
		this.date = date;
	}
	//Getter and setter
	public String getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(String patient_id) {
		this.patient_id = patient_id;
	}

	public java.sql.Timestamp getDate() {
		return date;
	}

	public void setDate(java.sql.Timestamp date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
