package classes;

import java.sql.Timestamp;;

public class Patient  {
	private String id;
	private String firstName;
	private String lastName;
	private String gender;
	private int age;
	private String phone;
	private Address address;
	private Timestamp date;
	private String room;
	private String description;
	private String price;
	private String doctor_id;
	
//Constructors
	public Patient() {
		super();
	}
	
	
	public Patient(String id, String firstName, String lastName, String gender, int age, String phone, Address address,
			Timestamp date, String room, String description, String price, String doctor_id) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.phone = phone;
		this.address = address;
		this.date = date;
		this.room = room;
		this.description = description;
		this.price = price;
		this.doctor_id = doctor_id;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender
				+ ", age=" + age + ", phone=" + phone + ", address=" + address + ", date=" + date + ", room=" + room
				+ ", description=" + description + ", price=" + price + ", doctor_id=" + doctor_id + "]";
	}
	//Getter and setter methods	
	public String getFirstName() {
		return firstName;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	
	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(String doctor_id) {
		this.doctor_id = doctor_id;
	}
	
	
}
