package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import javax.swing.table.DefaultTableModel;

import classes.Patient;
import classes.Receipt;

public class DatabaseReceipt extends Database{

	public DatabaseReceipt() {
		super();
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		
	}
	
	//Create A receipt record
	public void createRecord(Receipt reciept) {
		try {
			String query= "INSERT INTO receipts VALUES(NULL,?,?)";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, reciept.getPatient_id());
			pStatement.setTimestamp(2, reciept.getDate());
			pStatement.executeUpdate();
			
			System.out.println("You have insert a receipt record.");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	//Get all receipts
	public void getAllReceipts(DefaultTableModel modelReceipt) {
		try {
			String[] row= new String[5];
			String query= "SELECT r.*,p.price,p.first_name,p.last_name FROM patients AS p JOIN receipts AS r ON p.id LIKE r.patient_id order by r.id";
			Statement statement= super.getConnection().createStatement();
			ResultSet results= statement.executeQuery(query);
			while(results.next()) {
				int id= results.getInt(1);
				String patient_id=results.getString(2);
				Timestamp date= results.getTimestamp(3);
				String price= results.getString(4);
				String firstName= results.getString(5);
				String lastName= results.getString(6);
				
				
				row[0]= id+"";
				row[1]=patient_id;
				row[2]=firstName+"  "+lastName;
				row[3]=date+"";
				row[4]=price;
				modelReceipt.addRow(row);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public int getRecordID(String idPatient) {
		int rid=1;
		try {
			String query= "SELECT id FROM receipts WHERE patient_id LIKE ? ";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, idPatient);
			ResultSet result= pStatement.executeQuery();
			while(result.next()) {
				rid= result.getInt(1);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return rid;
	}
	//Get A record from receipt and patient table
	@SuppressWarnings("null")
	public Patient getReceipt(String id) {
		Patient patient=null;
		try {
			String query= "SELECT p.id,p.first_name,p.last_name,p.gender,p.age,p.phone,p.price,r.date FROM patients AS p JOIN receipts AS r ON p.id LIKE ?";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, id);
			ResultSet  results= pStatement.executeQuery();
			while(results.next()) {
				String idPatient=results.getString(1);
				String firstName=results.getString(2);
				String lastName=results.getString(3);
				String gender= results.getString(4);
				int age=results.getInt(5);
				String phone= results.getString(6);
				String price=results.getString(7);
				Timestamp date= results.getTimestamp(8);
				
				patient= new Patient(idPatient, firstName, lastName, gender, age, phone, null, date, null, null, price, null);
				System.out.println("You have got a record");
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return patient;
	}
}
