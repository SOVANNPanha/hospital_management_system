package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

import classes.Address;
import classes.Patient;

public class DatabasePatient extends Database{
	private DefaultTableModel modelPatient;
	private int indexTable;
	public DatabasePatient() {
		super();
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		DatabasePatient data= new DatabasePatient();
		
		data.closeConnection();
	}
	//Get all patients
	public void getAllPatients() {
		try {
			String[] row= new String[16];
			String query= "SELECT * FROM  patients where paid LIKE 'not yet' ";
			Statement stm= super.getConnection().createStatement();
			ResultSet results= stm.executeQuery(query);
			while(results.next()) {
				//Patient table
				String id= results.getString(1);
				String fname= results.getString(2);
				String lname= results.getString(3);
				String gender= results.getString(4);
				int age= results.getInt(5);
				String phone= results.getString(6);
				String villege= results.getString(7);
				String commune= results.getString(8);
				String city= results.getString(9);
				String province= results.getString(10);
				String country= results.getString(11);
				Address address= new Address(villege, commune, city, province, country);
				
				java.sql.Timestamp date= results.getTimestamp(12);
				String room= results.getString(13);
				String description= results.getString(14);
				String prix= results.getString(15);
				String doctor_id= results.getString(16);
				String paid= results.getString(17);
				Patient patient= new Patient(id, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
				
//				Add information into table of Patient and Appointment
					row[0]= id;
					row[1]= fname;
					row[2]= lname;
					row[3]= gender;
					row[4]= age+"";
					row[5]= date.toString();
					row[6]= room;
					row[7]= phone;
					row[8]= paid;
					modelPatient.addRow(row);
				}
			System.out.println("Get Patients succeed From DatabasePatient");
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	//update column paid value
	public void updatePaidColumnValue(String id) {
		try {
			String query= "UPDATE patients SET paid = 'paid' WHERE id LIKE ?";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, id);
			pStatement.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//get a patients by ID
	public Patient getPatientByID(String id) {
		Patient patient=null;
		try {
			String query= "SELECT * FROM  patients  WHERE id LIKE ? ";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, id);
			ResultSet results= pStatement.executeQuery();
			while(results.next()) {
				//Patient table
				String idPatient= results.getString(1);
				String fname= results.getString(2);
				String lname= results.getString(3);
				String gender= results.getString(4);
				int age= results.getInt(5);
				String phone= results.getString(6);
				String villege= results.getString(7);
				String commune= results.getString(8);
				String city= results.getString(9);
				String province= results.getString(10);
				String country= results.getString(11);
				Address address= new Address(villege, commune, city, province, country);
				
				java.sql.Timestamp date= results.getTimestamp(12);
				String room= results.getString(13);
				String description= results.getString(14);
				String prix= results.getString(15);
				String doctor_id= results.getString(16);

				patient= new Patient(idPatient, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
				System.out.println("Get Patients succeed From DatabasePatient");
			}
		} catch (Exception e) {
			System.out.println(e);
		}	
		return patient;
	}
		
	//Create a patient
	public boolean createPatient(Patient patient)  {
		boolean isCreated= false;
		try {
			String query = "INSERT INTO patients VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'not yet')";
			PreparedStatement pStatement;
			pStatement = super.getConnection().prepareStatement(query);
			pStatement.setString(1, patient.getId());
			pStatement.setString(2, patient.getFirstName());
			pStatement.setString(3, patient.getLastName());
			pStatement.setString(4, patient.getGender());
			pStatement.setInt(5, patient.getAge());
			pStatement.setString(6, patient.getPhone());
			pStatement.setString(7, patient.getAddress().getVillege());
			pStatement.setString(8, patient.getAddress().getCommune());
			pStatement.setString(9, patient.getAddress().getCity());
			pStatement.setString(10, patient.getAddress().getProvince());
			pStatement.setString(11, patient.getAddress().getCountry());
	
			pStatement.setTimestamp(12, patient.getDate());
			pStatement.setString(13, patient.getRoom());
			pStatement.setString(14, patient.getDescription());
			pStatement.setString(15, patient.getPrice());
			pStatement.setString(16, patient.getDoctor_id());

			if(pStatement.executeUpdate() > 0) {
				String[] row= new String[9];
				row[0]= patient.getId();
				row[1]= patient.getFirstName();
				row[2]= patient.getLastName();
				row[3]= patient.getGender();
				row[4]= patient.getAge()+"";
				row[5]= patient.getDate().toString();
				row[6]= patient.getRoom();
				row[7]= patient.getPhone();
				row[8]= "not yet";
				modelPatient.addRow(row);
				
				isCreated= true;
				System.out.println("You have insert a patient record");
			};
		}catch (Exception e) {
			System.out.println(e);
		}
		return isCreated;
	}
	
	//Update patient
	public boolean updatePatient(Patient p,String prev_PatientID) {
		boolean isUpdated= false;
		try {
			String query= "UPDATE patients SET id=?,first_name=?,last_name=?,gender=?,age=?,phone=?,villege=?,commune=?,city=?,province=?,country=?, date=?,room=?,description=?,price=?,doctor_id=? WHERE id LIKE ?";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, p.getId());
			pStatement.setString(2, p.getFirstName());
			pStatement.setString(3, p.getLastName());
			pStatement.setString(4, p.getGender());
			pStatement.setInt(5, p.getAge());
			pStatement.setString(6, p.getPhone());
			pStatement.setString(7, p.getAddress().getVillege());
			pStatement.setString(8, p.getAddress().getCommune());
			pStatement.setString(9, p.getAddress().getCity());
			pStatement.setString(10, p.getAddress().getProvince());
			pStatement.setString(11, p.getAddress().getCountry());
			
			pStatement.setTimestamp(12, p.getDate());
			pStatement.setString(13, p.getRoom());
			pStatement.setString(14, p.getDescription());
			pStatement.setString(15, p.getPrice());
			pStatement.setString(16, p.getDoctor_id());
			pStatement.setString(17, prev_PatientID);
			
			if(pStatement.executeUpdate() > 0) {
				modelPatient.removeRow(indexTable);
				
				String[] row= new String[9];
				row[0]= p.getId();
				row[1]= p.getFirstName();
				row[2]= p.getLastName();
				row[3]= p.getGender();
				row[4]= p.getAge()+"";
				row[5]= p.getDate().toString();
				row[6]= p.getRoom();
				row[7]= p.getPhone();
				row[8]= "not yet";
				modelPatient.addRow(row);
				isUpdated= true;
				System.out.println("Update Patient succeed");
			};
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return isUpdated;
	}
	
	//Delete a Patient record
	public void deletePatient(String id) {
		try {
			String query = "DELETE FROM patients WHERE id LIKE ?";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, id);
			if(pStatement.executeUpdate() > 0) {
				modelPatient.removeRow(indexTable);
				System.out.println("You have deleted a patient record");
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	//Search by Name of patient 
	public void getPatientByName(DefaultTableModel modelPatient,String firstname,String lastname) {
		try {
			String[] row= new String[9];
 			String query= "SELECT * FROM patients  WHERE first_name=? AND last_name=? AND paid LIKE 'not yet'";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1,firstname);
			pStatement.setString(2, lastname);
			ResultSet results= pStatement.executeQuery();
			while(results.next()) {
				//Patient table
				String id= results.getString(1);
				String fname= results.getString(2);
				String lname= results.getString(3);
				String gender= results.getString(4);
				int age= results.getInt(5);
				String phone= results.getString(6);
				String villege= results.getString(7);
				String commune= results.getString(8);
				String city= results.getString(9);
				String province= results.getString(10);
				String country= results.getString(11);
				Address address= new Address(villege, commune, city, province, country);
				
				java.sql.Timestamp date= results.getTimestamp(12);
				String room= results.getString(13);
				String description= results.getString(14);
				String prix= results.getString(15);
				String doctor_id= results.getString(16);
				String paid= results.getString(17);
				
				Patient patient= new Patient(id, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
			//Add information into table of Patient and Appointment

				row[0]= id+"";
				row[1]= fname;
				row[2]= lname;
				row[3]= gender;
				row[4]= age+"";
				row[5]= date.toString();
				row[6]= room;
				row[7]= phone;
				row[8]= paid;
				modelPatient.addRow(row);
			}
			System.out.println("You have get Patient By search Name");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//Get Patients By First Name
	public void getPatientByFirstName(DefaultTableModel modelPatient,String firstName) {
		try {
			String[] row= new String[9];
			String query= "SELECT * FROM patients WHERE first_name LIKE ? AND paid LIKE 'not yet'";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, firstName);
			ResultSet results= pStatement.executeQuery();
			while(results.next()) {
				//Patient table
				String id= results.getString(1);
				String fname= results.getString(2);
				String lname= results.getString(3);
				String gender= results.getString(4);
				int age= results.getInt(5);
				String phone= results.getString(6);
				String villege= results.getString(7);
				String commune= results.getString(8);
				String city= results.getString(9);
				String province= results.getString(10);
				String country= results.getString(11);
				Address address= new Address(villege, commune, city, province, country);
				
				java.sql.Timestamp date= results.getTimestamp(12);
				String room= results.getString(13);
				String description= results.getString(14);
				String prix= results.getString(15);
				String doctor_id= results.getString(16);
				String paid= results.getString(17);
				
				Patient patient= new Patient(id, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
			//Add information into table of Patient and Appointment
				row[0]= id+"";
				row[1]= fname;
				row[2]= lname;
				row[3]= gender;
				row[4]= age+"";
				row[5]= date.toString();
				row[6]= room;
				row[7]= phone;
				row[8]= paid;
				modelPatient.addRow(row);
			}
			System.out.println("You have Patients By First Name");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//Get patients By Last name
	public void getPatientByLastName(DefaultTableModel modelPatient,String lastName) {
		try {
			String[] row= new String[9];
			String query= "SELECT * FROM patients WHERE last_name LIKE ? AND paid LIKE 'not yet'";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, lastName);
			ResultSet results= pStatement.executeQuery();
			while(results.next()) {
				//Patient table
				String id= results.getString(1);
				String fname= results.getString(2);
				String lname= results.getString(3);
				String gender= results.getString(4);
				int age= results.getInt(5);
				String phone= results.getString(6);
				String villege= results.getString(7);
				String commune= results.getString(8);
				String city= results.getString(9);
				String province= results.getString(10);
				String country= results.getString(11);
				Address address= new Address(villege, commune, city, province, country);
				
				java.sql.Timestamp date= results.getTimestamp(12);
				String room= results.getString(13);
				String description= results.getString(14);
				String prix= results.getString(15);
				String doctor_id= results.getString(16);
				String paid= results.getString(17);
				
				Patient patient= new Patient(id, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
			//Add information into table of Patient and Appointment
				
				row[0]= id+"";
				row[1]= fname;
				row[2]= lname;
				row[3]= gender;
				row[4]= age+"";
				row[5]= date.toString();
				row[6]= room;
				row[7]= phone;
				row[8]= paid;
				modelPatient.addRow(row);
			}
			System.out.println("You got Patients By Last Namw");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//get Patients By Date
	public void getPatientByDate(DefaultTableModel modelPatient,String dateValue) {
		try {
			String[] row= new String[9];
			String query= "SELECT * FROM patients WHERE date LIKE ? AND paid LIKE 'not yet' ";
			PreparedStatement pStatement= super.getConnection().prepareStatement(query);
			pStatement.setString(1, dateValue);
			ResultSet results= pStatement.executeQuery();
			while(results.next()) {
				//Patient table
					String id= results.getString(1);
					String fname= results.getString(2);
					String lname= results.getString(3);
					String gender= results.getString(4);
					int age= results.getInt(5);
					String phone= results.getString(6);
					String villege= results.getString(7);
					String commune= results.getString(8);
					String city= results.getString(9);
					String province= results.getString(10);
					String country= results.getString(11);
					Address address= new Address(villege, commune, city, province, country);
					
					java.sql.Timestamp date= results.getTimestamp(12);
					String room= results.getString(13);
					String description= results.getString(14);
					String prix= results.getString(15);
					String doctor_id= results.getString(16);
					String paid= results.getString(17);
					
					Patient patient= new Patient(id, fname, lname, gender, age, phone, address, date, room, description, prix, doctor_id);
				//Add information into table of Patient and Appointment
					row[0]= id;
					row[1]= fname;
					row[2]= lname;
					row[3]= gender;
					row[4]= age+"";
					row[5]= date.toString();
					row[6]= room;
					row[7]= phone;
					row[8]= paid;
					modelPatient.addRow(row);
				}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public DefaultTableModel getModelPatient() {
		return modelPatient;
	}
	public void setModelPatient(DefaultTableModel modelPatient) {
		this.modelPatient = modelPatient;
	}
	public int getIndexTable() {
		return indexTable;
	}
	public void setIndexTable(int indexTable) {
		this.indexTable = indexTable;
	}
	
}
