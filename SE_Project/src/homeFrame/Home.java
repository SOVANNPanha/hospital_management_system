package homeFrame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

import classes.Doctor;
import classes.Patient;
import database.DatabaseDoctor;
import database.DatabasePatient;
import database.DatabaseReceipt;

import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

public class Home {

	private JFrame frmHospitalManagementSysytem;
	JPanel patient;
	JLabel patienOption;
	JPanel doctor;
	JLabel doctorOption;
	JPanel receipts;
	JLabel receiptOption;
	JButton btnCreatePatient;
	DatabasePatient databaseP;
	DatabaseDoctor databaseD;
	FormCreatePatient formCreatePatient;
	FormCreateDoctor formCreateDoctor;
	
	JTable tablePatient;
	DefaultTableModel modelPatient;
	JScrollPane scrollPanePatient;
	JTable doctorTable;
	DefaultTableModel modelDoctor;
	JScrollPane scrollPaneDoctor;
	
	JTable receiptTable;
	DefaultTableModel modelReceipt;
	JScrollPane scrollPaneReceipt;
	
	private JButton btnSearchName;
	private String signalOfPatient;
	private JTextField yearSearch;
	private JTextField monthSearch;
	private JTextField daySearch;
	private JButton btnSearchDate;
	private JButton btnRefresh;
	private JTextField firstNameSearch;
	private JTextField lastNameSearch;
	
	private String firstName;
	private String lastName;
	private String date;
	private JLabel lblSearchFname;
	private JLabel lblLastName;
	private JLabel lblNewLabel;
	private JLabel lblMm;
	private JLabel lblDd;
	
	/**
	 * Launch 
	 * 4/
	 * the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home window = new Home("getAllPatients");
					window.getFrame().setVisible(true);
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Home(String signal) {
		this.signalOfPatient = signal;
		initialize();
	}
	public Home(String signal,String firstName,String lastName) {
		this.signalOfPatient = signal;
		this.firstName=firstName;
		this.lastName= lastName;
		initialize();	
	}
	public Home(String signal,String date) {
		this.signalOfPatient = signal;
		this.date=date;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHospitalManagementSysytem = new JFrame();
		frmHospitalManagementSysytem.setTitle("Hospital Management System");
		frmHospitalManagementSysytem.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmHospitalManagementSysytem.setBounds(100, 100, 1200, 600);
		frmHospitalManagementSysytem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHospitalManagementSysytem.getContentPane().setLayout(null);
	
	//Block Side bar
		JPanel sidebar = new JPanel();
		sidebar.setBorder(new LineBorder(new Color(0, 0, 0)));
		sidebar.setBackground(new Color(64, 224, 208));
		sidebar.setForeground(Color.BLACK);
		sidebar.setBounds(0, 0, 184, 561);
		frmHospitalManagementSysytem.getContentPane().add(sidebar);
		sidebar.setLayout(null);
		
		 patienOption = new JLabel("Patients");
		patienOption.setForeground(Color.RED);
		patienOption.setBackground(Color.PINK);
		patienOption.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				patient.setVisible(true);
				patienOption.setForeground(Color.RED);
				doctor.setVisible(false);
				doctorOption.setForeground(Color.BLACK);
				receipts.setVisible(false);
				receiptOption.setForeground(Color.BLACK);
				
			}
		});
		patienOption.setFont(new Font("Tahoma", Font.BOLD, 14));
		patienOption.setHorizontalAlignment(SwingConstants.LEFT);
		patienOption.setBounds(10, 59, 125, 30);
		sidebar.add(patienOption);
		
		 doctorOption = new JLabel("Doctors");
		doctorOption.setBackground(Color.WHITE);
		doctorOption.setForeground(Color.BLACK);
		doctorOption.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				patient.setVisible(false);
				patienOption.setForeground(Color.BLACK);
				doctor.setVisible(true);
				doctorOption.setForeground(Color.RED);
				receipts.setVisible(false);
				receiptOption.setForeground(Color.BLACK);
				
			}
		});
		doctorOption.setHorizontalAlignment(SwingConstants.LEFT);
		doctorOption.setFont(new Font("Tahoma", Font.BOLD, 14));
		doctorOption.setBounds(10, 100, 125, 30);
		sidebar.add(doctorOption);
		
		 receiptOption = new JLabel("Receipts");
		 receiptOption.setBackground(Color.WHITE);
		 receiptOption.setForeground(Color.BLACK);
		receiptOption.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				patient.setVisible(false);
				patienOption.setForeground(Color.BLACK);
				doctor.setVisible(false);
				doctorOption.setForeground(Color.BLACK);
				receipts.setVisible(true);
				receiptOption.setForeground(Color.RED);
			}
		});
		receiptOption.setHorizontalAlignment(SwingConstants.LEFT);
		receiptOption.setFont(new Font("Tahoma", Font.BOLD, 14));
		receiptOption.setBounds(10, 141, 125, 30);
		sidebar.add(receiptOption);
		
		JLabel lblWelcome = new JLabel("Welcome");
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWelcome.setBounds(10, 11, 164, 30);
		sidebar.add(lblWelcome);
	//End of Block Side Bar
	//Block PetientOption 
		patient = new JPanel();
		patient.setBackground(new Color(64, 224, 208));
		patient.setBorder(new LineBorder(new Color(0, 0, 0)));
		patient.setBounds(184, 0, 1000, 561);
		frmHospitalManagementSysytem.getContentPane().add(patient);
		patient.setLayout(null);
		
		JLabel lblListOfPatients = new JLabel("List of Patients");
		lblListOfPatients.setBackground(Color.WHITE);
		lblListOfPatients.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblListOfPatients.setBounds(21, 11, 174, 30);
		patient.add(lblListOfPatients);
	
		 btnCreatePatient = new JButton("Add");
		 btnCreatePatient.setForeground(new Color(255, 255, 255));
		 btnCreatePatient.setEnabled(false);
		 btnCreatePatient.setVisible(false);
		btnCreatePatient.setBackground(new Color(0, 0, 255));
		btnCreatePatient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					 formCreatePatient = new FormCreatePatient();
					 formCreatePatient.getFramePatientForm().setVisible(true);
					 formCreatePatient.setModelPatient(modelPatient);
					 formCreatePatient.getBtnPrint().setVisible(false);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		btnCreatePatient.setBounds(856, 59, 125, 30);
		patient.add(btnCreatePatient);
		btnCreatePatient.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		tablePatient = new JTable();
		tablePatient.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int rowNumber= tablePatient.getSelectedRow();
				String idP = tablePatient.getValueAt(rowNumber, 0).toString();
				
				databaseP= new DatabasePatient();
				Patient patient= databaseP.getPatientByID(idP);
				databaseP.closeConnection();
				try {					
					FormCreatePatient formCreatePatient = new FormCreatePatient(patient);
					formCreatePatient.getFramePatientForm().setVisible(true);
					
					formCreatePatient.setModelPatient(modelPatient);
					formCreatePatient.setIndexTable(rowNumber);
					
					formCreatePatient.getPatient_id().setText(patient.getId());
					formCreatePatient.setPrev_PatientID(patient.getId());
					formCreatePatient.getFirstName().setText(patient.getFirstName());
					formCreatePatient.getLastName().setText(patient.getLastName());
					formCreatePatient.getGender().setText(patient.getGender());
					formCreatePatient.getAge().setText(patient.getAge()+"");
					formCreatePatient.getPhoneNumberP().setText(patient.getPhone());
					
				//Address
					formCreatePatient.getVillege().setText(patient.getAddress().getVillege());
					formCreatePatient.getCommune().setText(patient.getAddress().getCommune());
					formCreatePatient.getCity().setText(patient.getAddress().getCity());
					formCreatePatient.getProvinceP().setText(patient.getAddress().getProvince());
					formCreatePatient.getCountry().setText(patient.getAddress().getCountry());
				
					formCreatePatient.getRoomP().setText(patient.getRoom());
					formCreatePatient.getDescriptionA().setText(patient.getDescription());
					formCreatePatient.getDoctor_id().setText(patient.getDoctor_id());
					
				//Change The value of the Button
					formCreatePatient.getBtnDeletePatient().setVisible(true);
					formCreatePatient.getBtnCreatePatient().setText("Update");
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		tablePatient.setBackground(Color.WHITE);
		String[] columnPatient= {"ID","First Name","Last Name","Gender","Age","Date","Room","Phone Contact","Paid"};
		modelPatient= new DefaultTableModel();
		tablePatient.setModel(modelPatient);
		modelPatient.setColumnIdentifiers(columnPatient);
		try {
			databaseP= new DatabasePatient();
			databaseP.setModelPatient(modelPatient);
			switch (getSignalOfPatient()) {
				case "getAllPatients":
					getBtnCreatePatient().setVisible(true);
					getBtnCreatePatient().setEnabled(true);
					databaseP.getAllPatients();
					System.out.println("In the getAllPatients Block");
					break;
				case "getPatientByName":
					databaseP.getPatientByName(modelPatient, this.firstName,this.lastName);
					btnCreatePatient.setVisible(false);
					break;
				case "getPatientByDate":
					databaseP.getPatientByDate(modelPatient, this.date);
					btnCreatePatient.setVisible(false);
					break;
				case "getPatientByFirstName":
					databaseP.getPatientByFirstName(modelPatient, firstName);
					btnCreatePatient.setVisible(false);
					break;
				case "getPatientByLastName":
					databaseP.getPatientByLastName(modelPatient, lastName);
					btnCreatePatient.setVisible(false);
					break;
				default:
					break;
			}
			databaseP.closeConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
		 scrollPanePatient = new JScrollPane(tablePatient);
		scrollPanePatient.setBounds(21, 100, 960, 450);
		patient.add(scrollPanePatient);
		
		firstNameSearch = new JTextField();
		firstNameSearch.setFont(new Font("Tahoma", Font.PLAIN, 14));
		firstNameSearch.setBounds(21, 60, 139, 30);
		patient.add(firstNameSearch);
		firstNameSearch.setColumns(10);
		
		btnSearchName = new JButton("Search");
		btnSearchName.setForeground(new Color(255, 255, 255));
		btnSearchName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(firstNameSearch.getText().isEmpty() && lastNameSearch.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "What the name of Patient do You want to Search?");
				}else {
					if(lastNameSearch.getText().isEmpty()) {
						Home homeSearchName= new Home("getPatientByFirstName",firstNameSearch.getText(),"");
						homeSearchName.getFrame().setVisible(true);
						homeSearchName.getBtnCreatePatient().setVisible(false);
						getFrame().dispose();
					}else if(firstNameSearch.getText().isEmpty()) {
						Home homeSearchName= new Home("getPatientByLastName","",lastNameSearch.getText());
						homeSearchName.getFrame().setVisible(true);
						homeSearchName.getBtnCreatePatient().setVisible(false);
						getFrame().dispose();
					}else {
						Home homeSearchName= new Home("getPatientByName",firstNameSearch.getText(),lastNameSearch.getText());
						homeSearchName.getFrame().setVisible(true);
						homeSearchName.getBtnCreatePatient().setVisible(false);
						getFrame().dispose();
					}
				}
			}
		});
		btnSearchName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSearchName.setBackground(new Color(0, 0, 255));
		btnSearchName.setBounds(320, 59, 87, 30);
		patient.add(btnSearchName);
		
		yearSearch = new JTextField();
		yearSearch.setHorizontalAlignment(SwingConstants.CENTER);
		yearSearch.setFont(new Font("Tahoma", Font.PLAIN, 14));
		yearSearch.setColumns(10);
		yearSearch.setBounds(447, 59, 50, 30);
		patient.add(yearSearch);
		
		monthSearch = new JTextField();
		monthSearch.setHorizontalAlignment(SwingConstants.CENTER);
		monthSearch.setFont(new Font("Tahoma", Font.PLAIN, 14));
		monthSearch.setColumns(10);
		monthSearch.setBounds(507, 59, 50, 30);
		patient.add(monthSearch);
		
		daySearch = new JTextField();
		daySearch.setHorizontalAlignment(SwingConstants.CENTER);
		daySearch.setFont(new Font("Tahoma", Font.PLAIN, 14));
		daySearch.setColumns(10);
		daySearch.setBounds(567, 59, 50, 30);
		patient.add(daySearch);
		
		btnSearchDate = new JButton("Search");
		btnSearchDate.setForeground(new Color(255, 255, 255));
		btnSearchDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(yearSearch.getText().isEmpty() || monthSearch.getText().isEmpty() || daySearch.getText().isEmpty() ) {
					JOptionPane.showMessageDialog(null, "What date do You want to search?");
				}else {
					String year= yearSearch.getText();
					String month= monthSearch.getText();
					String day= daySearch.getText();
					if(month.length() ==1) {
						month= "0"+month;
					}
					String date=year +"-"+ month+"-"+day+"%"; 
					Home homeSearchDate= new Home("getPatientByDate",date);
					homeSearchDate.getFrame().setVisible(true);
					homeSearchDate.getBtnCreatePatient().setVisible(false);
					getFrame().dispose();
				}
			}
		});
		btnSearchDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSearchDate.setBackground(new Color(0, 0, 255));
		btnSearchDate.setBounds(627, 58, 87, 30);
		patient.add(btnSearchDate);
				
		btnRefresh = new JButton("Refresh");
		btnRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					getFrame().dispose();
					getBtnCreatePatient().setVisible(true);
					Home home= new Home("getAllPatients");
					home.getFrame().setVisible(true);
				} catch (Exception e2) {
					System.out.println(e2);
				}
			}
		});
		
		btnRefresh.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRefresh.setBackground(new Color(255, 255, 255));
		btnRefresh.setBounds(856, 11, 125, 30);
		patient.add(btnRefresh);
		
		lastNameSearch = new JTextField();
		lastNameSearch.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lastNameSearch.setColumns(10);
		lastNameSearch.setBounds(171, 60, 139, 30);
		patient.add(lastNameSearch);
		
		lblSearchFname = new JLabel("First Name");
		lblSearchFname.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSearchFname.setBounds(21, 42, 100, 20);
		patient.add(lblSearchFname);
		
		lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLastName.setBounds(172, 42, 100, 20);
		patient.add(lblLastName);
		
		JLabel lblYyyy = new JLabel("YYYY");
		lblYyyy.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblYyyy.setBounds(448, 39, 50, 20);
		patient.add(lblYyyy);
		
		lblMm = new JLabel("MM");
		lblMm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMm.setBounds(507, 40, 50, 20);
		patient.add(lblMm);
		
		lblDd = new JLabel("DD");
		lblDd.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDd.setBounds(567, 39, 50, 20);
		patient.add(lblDd);
//	//End of Block PatientOption	
//	//Block doctorOption 
		doctor = new JPanel();
		doctor.setBackground(new Color(64, 224, 208));
		doctor.setBorder(new LineBorder(new Color(0, 0, 0)));
		doctor.setBounds(184, 0, 1000, 561);
		frmHospitalManagementSysytem.getContentPane().add(doctor);
		doctor.setLayout(null);
		
		JButton addDoctorbtn;
		addDoctorbtn = new JButton("Add Doctor");
		addDoctorbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					 formCreateDoctor = new FormCreateDoctor();
					 formCreateDoctor.getFrame().setVisible(true);
					 formCreateDoctor.setModelD(modelDoctor);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		});
		addDoctorbtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		addDoctorbtn.setBounds(856, 59, 125, 30);
		doctor.add(addDoctorbtn);
		
		JLabel labelDoctor = new JLabel("List of Doctors");
		labelDoctor.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelDoctor.setBounds(20, 11, 200, 30);
		doctor.add(labelDoctor);
		
		 doctorTable= new JTable();
		 doctorTable.addMouseListener(new MouseAdapter() {
		 	@Override
		 	public void mouseClicked(MouseEvent e) {
		 		int row= doctorTable.getSelectedRow();
		 		String getID=  (String) modelDoctor.getValueAt(row, 0);// We get value from JTable is the String Object
		 		try {
		 			databaseD= new DatabaseDoctor();
					Doctor doctor= databaseD.getDoctorById(getID);
					databaseD.closeConnection();
					
					formCreateDoctor= new FormCreateDoctor(doctor);
					formCreateDoctor.getFrame().setVisible(true);
					formCreateDoctor.setIndexTable(row);
					formCreateDoctor.setModelD(modelDoctor);
					
					formCreateDoctor.getDoctor_id().setText(doctor.getId());
					formCreateDoctor.setPrev_DoctorID(doctor.getId());
					formCreateDoctor.getFirstD().setText(doctor.getFirstName());
					formCreateDoctor.getLastD().setText(doctor.getLastName());
					formCreateDoctor.getGenderD().setText(doctor.getGender());
					formCreateDoctor.getAgeD().setText(doctor.getAge()+"");
					formCreateDoctor.getSkillD().setText(doctor.getSkill());
					
					formCreateDoctor.getBtnCreate().setText("Update");
					formCreateDoctor.getBtnDelete().setVisible(true);
					formCreateDoctor.getTitleFormDoctor().setText("Dortor Form");
				} catch (Exception e2) {
					System.out.println(e);
				}
		 	}
		 });
		doctorTable.setFont(new Font("Tahoma", Font.PLAIN, 14));
		doctorTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		doctorTable.setBackground(Color.WHITE);
		
		String[] columnDoctor= {"ID","First Name","Last Name","Gender","Age","Skill"};
		 modelDoctor= new DefaultTableModel();
		doctorTable.setModel(modelDoctor);
		modelDoctor.setColumnIdentifiers(columnDoctor);
		try {
			databaseD= new DatabaseDoctor();
			databaseD.setModelDoctor(modelDoctor);
			databaseD.getAllDoctor();
			databaseD.closeConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
		scrollPaneDoctor = new JScrollPane(doctorTable);
		scrollPaneDoctor.setBounds(21, 100, 960, 450);
		doctor.add(scrollPaneDoctor);
		
//End of Block DoctorOption
//Block Receipt
		receipts = new JPanel();
		receipts.setBackground(new Color(64, 224, 208));
		receipts.setBorder(new LineBorder(new Color(0, 0, 0)));
		receipts.setBounds(184, 0, 1000, 561);
		frmHospitalManagementSysytem.getContentPane().add(receipts);
		receipts.setLayout(null);
		
		lblNewLabel = new JLabel("Receipt List");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(21, 11, 200, 34);
		receipts.add(lblNewLabel);
		
		 receiptTable= new JTable();
		 receiptTable.addMouseListener(new MouseAdapter() {
			@Override
		 	public void mouseClicked(MouseEvent arg0) {
		 		int rowNumber= receiptTable.getSelectedRow();
		 		String id= receiptTable.getValueAt(rowNumber, 1).toString();
		 		System.out.println(id);
		 		DatabaseReceipt databaseReceipt= new DatabaseReceipt();
		 		Patient patientGet=databaseReceipt.getReceipt(id);
		 		databaseReceipt.closeConnection();
		 		
		 		RecieptForm formReceipt = new RecieptForm();
		 		formReceipt.getFrame().setVisible(true);
	
		 		formReceipt.getP_id().setText(patientGet.getId());
		 		formReceipt.getP_first().setText(patientGet.getFirstName());
		 		formReceipt.getP_last().setText(patientGet.getLastName());
		 		formReceipt.getP_gender().setText(patientGet.getGender());
		 		formReceipt.getP_age().setText(patientGet.getAge()+"");
		 		formReceipt.getP_phone().setText(patientGet.getPhone());
		 		formReceipt.getP_prix().setText(patientGet.getPrice());
		 		formReceipt.getR_date().setText(patientGet.getDate().toString());

		 	}
		 });
		receiptTable.setBounds(22, 73, 968, 478);
		receiptTable.setFont(new Font("Tahoma", Font.PLAIN, 14));
		receiptTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		receiptTable.setBackground(Color.WHITE);
		
		String[] column= {"ID","Patient ID","Name","Date","Prix"};
		 modelReceipt= new DefaultTableModel();
		receiptTable.setModel(modelReceipt);
		modelReceipt.setColumnIdentifiers(column);
		try {
			DatabaseReceipt databaseReceipt= new DatabaseReceipt();
			databaseReceipt.getAllReceipts(modelReceipt);
			databaseReceipt.closeConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
		
		 scrollPaneReceipt = new JScrollPane(receiptTable);
		scrollPaneReceipt.setBounds(21, 100, 968, 478);
		receipts.add(scrollPaneReceipt);
		
		
	}
	public JFrame getFrame() {
		return frmHospitalManagementSysytem;
	}

	public void setFrame(JFrame frame) {
		this.frmHospitalManagementSysytem = frame;
	}

	public String getSignalOfPatient() {
		return signalOfPatient;
	}

	public void setSignalOfPatient(String signal) {
		this.signalOfPatient = signal;
	}

	public JTextField getYearSearch() {
		return yearSearch;
	}

	public void setYearSearch(JTextField yearSearch) {
		this.yearSearch = yearSearch;
	}

	public JTextField getMonthSearch() {
		return monthSearch;
	}

	public void setMonthSearch(JTextField monthSearch) {
		this.monthSearch = monthSearch;
	}

	public JTextField getDaySearch() {
		return daySearch;
	}

	public void setDaySearch(JTextField daySearch) {
		this.daySearch = daySearch;
	}

	public JTextField getFirstNameSearch() {
		return firstNameSearch;
	}

	public void setFirstNameSearch(JTextField firstNameSearch) {
		this.firstNameSearch = firstNameSearch;
	}

	public JTextField getLastNameSearch() {
		return lastNameSearch;
	}

	public void setLastNameSearch(JTextField lastNameSearch) {
		this.lastNameSearch = lastNameSearch;
	}

	public JButton getBtnCreatePatient() {
		return btnCreatePatient;
	}

	public void setBtnCreatePatient(JButton btnCreatePatient) {
		this.btnCreatePatient = btnCreatePatient;
	}

	public JFrame getFrmHospitalManagementSysytem() {
		return frmHospitalManagementSysytem;
	}

	public void setFrmHospitalManagementSysytem(JFrame frmHospitalManagementSysytem) {
		this.frmHospitalManagementSysytem = frmHospitalManagementSysytem;
	}
}
